Meitei script
https://www.loc.gov/catdir/cpso/romanization/romguide/Modern-Manipuri-Meitei-script-2022.pdf

https://www.omniglot.com/writing/meitei.htm
Vowels
ꯑ	◌
ꯑꯥ	ꯥꯥ
ꯏ	ꯤ 
ꯎ	ꯨ
ꯑꯦ	ꯦ
ꯑꯩ	ꯩ
ꯑꯣ	ꯣ 
ꯑꯧ	ꯧ

Consonants
ꯀ	ka
ꯁ	sa
ꯂ	la
ꯃ	ma
ꯄ	pa
ꯅ	na
ꯆ	ca
ꯇ	ta
ꯈ	kha
ꯉ	ṅa 
ꯊ	tha
ꯋ	va
ꯌ	ya
ꯍ	ha
ꯐ	pha
ꯒ	ga
ꯓ	jha
ꯔ	ra
ꯕ	ba
ꯖ	ja
ꯗ	da
ꯘ	gha
ꯙ	dha
ꯚ	bha

Final-consonants
ꯛ	k
ꯜ	l
ꯝ	m
ꯞ	p
ꯟ	n
ꯠ	t
ꯡ	ṅ

Numerals
꯰	0 
꯱	1 
꯲	2 
꯳	3
꯴	4 
꯵	5
꯶	6
꯷	7
꯸	8
꯹	9

Punctuation
꫱	question mark
꫰	comma
꯫	full stop/period
꯭ 	Apun iyek/Halanta
꯬	Heavy tone 

Bengali
Vowels	
অ	[ə]	
আ	[a]	
ই	[i]	
ঈ	[iː]	
উ	[u]	
ঊ	[uː]
ঋ	[ɽ]
এ	[e]	
ঐ	[ɐj]	
ও	[o]	
ঔ	[ɐw]	
অং	[əŋ]	
অঃ	[əh]

Vowel diacritics
ক	[kə]		
কা	[ka]	
কি	[ki]	
কী	[kiː]	
কু	[ku]	
কূ	[kuː]	
ঋ	[kɽi]
কে	[ke]	
কৈ	[kɐj]	
কো	[ko]	
কৌ	[kɐw]	
কং	[kəŋ]
কঃ	[kəh]

Consonants
ক	[k]	
খ	[kʰ]	
গ	[g]	
ঘ	[gʱ]	
ঙ	[ŋ]	
চ	[t͡ʃ]	
ছ	[t͡ʃʱ]	
জ	[d͡ʒ]	
ঝ	[d͡ʒʱ]	
ঞ	[ɲ]
ট	[ʈ]	
ঠ	[ʈʰ]	
ড	[ɖ]	
ঢ	[ɖʱ]	
ণ	[ɳ]	
ত	[t̪]	
থ	[t̪ʰ]	
দ	[d̪]	
ধ	[d̪ʱ]	
ন	[n]
প	[p]	
ফ	[pʰ]	
ব	[b]	
ভ	[bʱ]	
ম	[m]	
য়	[j]	
র	[r]	
ল	[l]
ৱ	[w]	
শ	[ʃ]
য	[ʂ]	
স	[s]	
হ	[ɦ]






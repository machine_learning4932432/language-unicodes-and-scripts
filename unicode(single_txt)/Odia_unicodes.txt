Various signs
◌ଁ U+0B01
ଂ U+0B02
ଃ U+0B03

Independent vowels
ଅ U+0B05
ଆ U+0B06
ଇ U+0B07
ଈ U+0B08
ଉ U+0B09
ଊ U+0B0A
ଋ U+0B0B
ଌ U+0B0C
ଏ U+0B0F
ଐ U+0B10
ଓ U+0B13
ଔ U+0B14

Consonants
କ U+0B15
ଖ U+0B16
ଗ U+0B17
ଘ U+0B18
ଙ U+0B19
ଚ U+0B1A
ଛ U+0B1B
ଜ U+0B1C
ଝ U+0B1D
ଞ U+0B1E
ଟ U+0B1F
ଠ U+0B20
ଡ U+0B21
ଢ U+0B22
ଣ U+0B23
ତ U+0B24
ଥ U+0B25
ଦ U+0B26
ଧ U+0B27
ନ U+0B28
ପ U+0B2A
ଫ U+0B2B
ବ U+0B2C
ଭ U+0B2D
ମ U+0B2E
ଯ U+0B2F
ର U+0B30
ଲ U+0B32
ଳ U+0B33
ଵ U+0B35
ଶ U+0B36
ଷ U+0B37
ସ U+0B38
ହ U+0B39

Various signs
◌଼ U+0B3C
ଽ U+0B3D

Dependent vowel signs
ା U+0B3E
◌ି U+0B3F
ୀ U+0B40
◌ୁ U+0B41
◌ୂ U+0B42
ୃ U+0B43
ୄ U+0B44
େ U+0B47
ୈ U+0B48

Two-part dependent vowel signs
ୋ U+0B4B
ୌ U+0B4C

Virama
୍ U+0B4D

Various signs
U+0B55
◌ୖ U+0B56
ୗ U+0B57

Additional consonants
ଡ଼ U+0B5C
ଢ଼ U+0B5D
ୟ U+0B5F

Additional vowels for Sanskrit
ୠ U+0B60
ୡ U+0B61

Dependent vowels
ୢ U+0B62
ୣ U+0B63

Digits
୦ U+0B66
୧ U+0B67
୨ U+0B68
୩ U+0B69
୪ U+0B6A
୫ U+0B6B
୬ U+0B6C
୭ U+0B6D
୮ U+0B6E
୯ U+0B6F

Sign
୰ U+0B70

Additional consonant
ୱ U+0B71

Fraction signs
୲ U+0B72
୳ U+0B73
୴ U+0B74
୵ U+0B75
୶ U+0B76
୷ U+0B77
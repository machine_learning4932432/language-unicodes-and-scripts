Various signs
◌ஂ U+0B82
ஃ U+0B83

Independent vowels
அ U+0B85
ஆ U+0B86
இ U+0B87
ஈ U+0B88
உ U+0B89
ஊ U+0B8A
எ U+0B8E
ஏ U+0B8F
ஐ U+0B90
ஒ U+0B92
ஓ U+0B93
ஔ U+0B94

Consonants
க U+0B95
ங U+0B99
ச U+0B9A
ஜ U+0B9C
ஞ U+0B9E
ட U+0B9F
ண U+0BA3
த  U+0BA4
ந U+0BA8
ன U+0BA9
ப U+0BAA
ம U+0BAE
ய U+0BAF
ர U+0BB0
ற U+0BB1
ல U+0BB2
ள U+0BB3
ழ U+0BB4
வ U+0BB5
ஶ U+0BB6
ஷ U+0BB7
ஸ U+0BB8
ஹ U+0BB9

Dependent vowel signs
ா U+0BBE
ி U+0BBF
ீ U+0BC0
ு U+0BC1
ூ U+0BC2
ெ U+0BC6
ே U+0BC7
ை U+0BC8

Two-part dependent vowel signs
 ொ U+0BCA
 ோ U+0BCB
 ௌ U+0BCC

Virama
◌் U+0BCD

Various signs
ௐ U+0BD0
ௗ U+0BD7

Digits
௦ U+0BE6
௧ U+0BE7
௨ U+0BE8
௩ U+0BE9
௪ U+0BEA
௫ U+0BEB
௬ U+0BEC
௭ U+0BED
௮ U+0BEE
௯ U+0BEF

Tamil numerics
௰ U+0BF0
௱ U+0BF1
௲ U+0BF2

Tamil calendrical symbols
௳ U+0BF3
௴ U+0BF4
௵ U+0BF5

Tamil clerical symbols
௶ U+0BF6
௷ U+0BF7
௸ U+0BF8

Currency symbol
௹ U+0BF9

Tamil clerical symbol
௺ U+0BFA
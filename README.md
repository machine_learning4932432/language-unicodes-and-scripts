# Language-Unicodes-and-Scripts

## References / Sources
### Unicode
- [https://home.unicode.org/](https://home.unicode.org/)
- [https://www.compart.com/en/unicode/](https://www.compart.com/en/unicode/)

### Language Scripts
- Bodo: [https://omniglot.com/writing/bodo.htm](https://omniglot.com/writing/bodo.htm)
- Dogri: [https://omniglot.com/writing/dogri.htm](https://omniglot.com/writing/dogri.htm)
- Mahajani:[https://www.omniglot.com/writing/mahajani.htm](https://www.omniglot.com/writing/mahajani.htm)
- Kashmiri: [https://omniglot.com/writing/kashmiri.htm](https://omniglot.com/writing/kashmiri.htm)
- Konkani: [https://omniglot.com/writing/konkani.htm](https://omniglot.com/writing/konkani.htm)
- Maithili: 
    - [https://omniglot.com/writing/maithili.htm](https://omniglot.com/writing/maithili.htm)
    - [https://www.omniglot.com/writing/tirhuta.htm](https://www.omniglot.com/writing/tirhuta.htm)
- Santali:
    - [https://omniglot.com/writing/santali.htm](https://omniglot.com/writing/santali.htm)
    - [Translating India's constitution into a tribal language](https://www.inkl.com/glance/news/translating-india-s-constitution-into-a-tribal-language?section=good-news) 
- Manipuri:
    - [https://www.loc.gov/catdir/cpso/romanization/romguide/Modern-Manipuri-Meitei-script-2022.pdf](https://www.loc.gov/catdir/cpso/romanization/romguide/Modern-Manipuri-Meitei-script-2022.pdf)
    - [https://www.omniglot.com/writing/meitei.htm](https://www.omniglot.com/writing/meitei.htm)
